The CHproject.py file includes four functions that can be used to calculate the different aspects of the complexity-entropy causality plane. These functions are: CH, C_min, C_max and CH_fbm.

The function CH(x,n, tau=1) takes as input a time-series x, the embedding dimension n and a possible embedding delay tau. 
It returns an array of 2 values, of which the first index has the C value and the second the H value.

The function C_max(n) takes as input the embedding dimension n.
It returns a two-dimensional array containing the CH values of the maximal complexity curve. The first subarray contains the C values, the second the H values.

The function C_min(n) takes as input the embedding dimension n.
It returns a two-dimensional array containing the CH values of the minimal complexity curve. The first subarray contains the C values, the second the H values.

The function CH_fbm(n) takes as input the embedding dimension n.
It returns a two-dimensional array containing the CH values of the fractional brownian motion curve. The first subarray contains the C values, the second the H values.

In the notebook CH-NumberOfSunpots.ipynb these functions are applied to three time series of the coutn of number of sunspots.
