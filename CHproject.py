#!/usr/bin/env python
# coding: utf-8

# In[ ]:


###############################################################################
### Given time-series x, and embedding dimension n, and embedding delay tau,###
### returns the Shannon entropy and Statistical Complexity.                 ###
###############################################################################
def CH(x,n,tau = 1):
    import numpy as np
    import math
    from collections import Counter

    # Embedding delay: only need the element at locations k*tau:
    length = len(x)//tau
    x_temp = x
    x = [x_temp[i*tau] for i in range(length)]

    
    #######################################################
    ### The following code computes the Shannon entropy ###
    ### of a time-series x with embedding dimension n.  ###
    #######################################################

    permutation = []         # list which will contain all different permutations occuring in list x
    N= math.factorial(n)
    S_max= np.log2(N)
    
    # these functions return respectively the first, second element of a list
    def takeFirst(elem):
        return elem[0]
    def takeSecond(elem):
        return elem[1]
    
    # Computes the Shannon entropy using p
    def entropy(p):
        som = 0
        for i in p:
            if i > 0:
                som += i*np.log2(i)
        return -som

    # Go over all elements in x, substract a sublist of dimension n, 
    # sort this list and extract the order of permutation
    for i in range(len(x)-(n-1)):
            sublist= x[i:i+n]
            keys = [item for item in range(1, n+1)] 
            perm = list(zip(keys, sublist)) 
            perm.sort(key=takeSecond)
            permutation.append([takeFirst(val) for val in perm])

    # count how often each permutation occurs
    count= (Counter(str(elem) for elem in permutation))
    p = np.zeros(len(count))      # list which will contain all frequencies of permutations                   

    i=0
    for char in count:
        p[i]=count[char]
        i+=1
    
    # calculate the relative frequency and calculate the shannon entropy
    p = p/(len(x)-n+1)
    S = entropy(p)
    H = S/S_max # The normalized shannon entropy


    ##############################################################
    ### The following code computes the statistical complexity ###
    ##############################################################
        
    # Fill a list with the equiprobable distribution of 1/N    
    p_e= np.zeros(N)
    p_e += (1/N)
    
    # Fill the probability distribution with additional zeros to the size of N
    p_temp = p
    p = np.zeros(N)
    p[0:len(p_temp)] = p_temp
    
    # Calculate Desequilibrium and normalization constant
    Q = entropy((p+p_e)/2) - (1/2)*entropy(p) - (1/2)*entropy(p_e)
    norm =  ((N+1)/N) * np.log2(N+1) - 2*np.log2(2*N) + np.log2(N)
    
    # Finally compute the statistical complexity
    C = -2*Q*H/norm
    return (C,H)


### This script consists out of two functions. 
### One calculates the maximal complexity curve, 
### the other the minimal complexity curve for a given embedding dimension n.
### These curves are returned as lists with the C and H values.

def C_max(n):
    import numpy as np
    import math
    
    N= math.factorial(n)

    f_max = np.linspace(1/N,1, 1000)

    H_max = np.zeros(len(f_max))
    C_max = np.zeros(len(f_max))

    for i in range(len(f_max)):
        # Fill a list with the equiprobable distribution of 1/N    
        p_e= np.zeros(N)
        p_e += (1/N)

        # Fill the probability distribution
        p = np.zeros(N)
        p[0] = f_max[i]
        p[1:N] = (1-f_max[i])/(N-1)


        def entropy(p):
                som = 0
                for i in p:
                    if i > 0:
                        som += i*np.log2(i)
                return -som

        # Calculate Entropy
        H_max[i] = entropy(p)/np.log2(N)
        
        # Calculate Desequilibrium and normalization constant
        Q = entropy((p+p_e)/2) - (1/2)*entropy(p) - (1/2)*entropy(p_e)
        norm =  ((N+1)/N) * np.log2(N+1) - 2*np.log2(2*N) + np.log2(N)

        # Finally compute the statistical complexity
        C_max[i] = -2*Q*H_max[i]/norm

    # Sort the values according to the magnitude of H for plotting purposes.
    c = C_max
    h = H_max
    C_max = [c for (h,c) in sorted(zip(h,c), key=lambda pair: pair[0])]
    H_max = [h for (h,c) in sorted(zip(h,c), key=lambda pair: pair[0])]
     
    return C_max,H_max


def C_min(n):
    import numpy as np
    import math
    
    def entropy(p):
        som = 0
        for i in p:
            if i > 0:
                som += i*np.log2(i)
        return -som
    
    N= math.factorial(n)

    H_min = []
    C_min = []

    for i in range(0,N-1):
        # Fill a list with the equiprobable distribution of 1/N    
        p_e= np.zeros(N)
        p_e += (1/N)
        
        fmin = np.linspace(0,1/(N-i), 60)

        for f in fmin:
            # Fill the probability distribution
            p = np.zeros(N)
            p[:i] = 0
            p[i] = f
            p[i+1:] = (1-f)/(N-i-1)

            # Calculate Entropy
            H_min.append(entropy(p)/np.log2(N))
            
            # Calculate Desequilibrium and normalization constant
            Q = entropy((p+p_e)/2) - (1/2)*entropy(p) - (1/2)*entropy(p_e)
            norm =  ((N+1)/N) * np.log2(N+1) - 2*np.log2(2*N) + np.log2(N)

            # Finally compute the statistical complexity
            C_min.append(-2*Q*H_min[-1]/norm)

    return C_min,H_min


# To compare let us also add the fractional brownian curve
def CH_fbm(n):
    from fbm import FBM
    import numpy as np
    import matplotlib.pyplot as plt

    alpha = np.arange(1.1,2.9,0.1)
    H = (alpha - 1)/2
    T = 10**5                   # Length of brownian motion

    f = []
    fbm_sample = []
    fbm = []
    for h in range(len(H)):
        f.append(FBM(n=T, hurst=H[h], length=T, method='daviesharte'))
        # Generate a fBm realization
        fbm_sample.append(f[h].fbm())

        # Calculate CH value of the fbm
        fbm.append(CH(fbm_sample[h],n))

    # sort the CH values from H=0 to H=1 for plotting purposes
    fbm = dict(fbm)  
    fbm = sorted(fbm.items(), key=lambda x: x[1])
    fbm = np.array(fbm)
    
    
    from scipy.ndimage import gaussian_filter1d
    smooth = gaussian_filter1d(fbm, 1, axis=0)

    fbm = smooth

    return fbm

